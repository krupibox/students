import React from 'react';
import { render } from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';

import { Layout, Table, Row, Col, Divider } from 'antd';

import {
  studentsRow,
  studentsCol,
  groupsRow,
  groupsCol
} from './mocks';

const App = () => {

  const { Content } = Layout;

  return (
    <>
      <Layout style={{ height: "100vh" }}>
        <Content
          className="site-layout-background"
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
          }}>

          <Divider>List of groups</Divider>

          <Row>
            <Col span={12} offset={6}>
              <Table columns={groupsCol} dataSource={groupsRow} pagination={false} />
            </Col>
          </Row>

          <Divider>List of students</Divider>

          <Row>
            <Col span={12} offset={6}>
              <Table columns={studentsCol} dataSource={studentsRow} pagination={false} />
            </Col>
          </Row>

        </Content>
      </Layout>
    </>
  );
};

render(<App />, document.getElementById('root'));
