export const studentsRow
 = [
    {
        key: '1',
        name: 'Mike',
        surname: 'Jordan',
        birthday: '15.10.2001',
        groupNumber: 5,
    },
];

export const studentsCol = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Surname',
        dataIndex: 'surname',
        key: 'surname',
    },
    {
        title: 'Birthday',
        dataIndex: 'birthday',
        key: 'birthday',
    },
    {
        title: 'Group number',
        dataIndex: 'groupNumber',
        key: 'groupNumber',
    },
];

export const groupsRow
 = [
    {
        key: '1',
        groupNumber: '1',
    },
    {
        key: '2',
        groupNumber: '2',
    },
    {
        key: '3',
        groupNumber: '3',
    },
];

export const groupsCol = [
    {
        title: 'Groups',
        dataIndex: 'groupNumber',
        key: 'groupNumber',
    },
];